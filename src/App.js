import React, { useState } from "react";
import Task from "./components/Task";
import AddNewTask from "./components/AddNewTask";
import SearchBox from "./components/SearchBox";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button } from "react-bootstrap";
import Edit from "./components/Edit";
import "./style.css";

function App() {
  const [todolist, setTodoList] = useState([
    {
      id: new Date(),
      task: "",
      description: "",
      priority: "",
      deadline: "",
      done: "",
    },
  ]);
  const [searchTask, setSearchTask] = useState("");

  const editChange = (e, id) => {
    const editedTodolist = todolist.map((item) => (item.id === id ? e : item));

    setTodoList(editedTodolist);
  };

  const addTodo = (fields) => {
    const newTask = {
      ...fields,
      id: new Date(),
    };
    setTodoList(todolist.concat(newTask));
  };

  const deleteTodo = (id) => {
    const delTodolist = todolist.filter((item) => id !== item.id);
    setTodoList(delTodolist);
  };

  const handleInput = (e) => {
    setSearchTask(e.target.value);
  };

  const filteredTask = todolist.filter((item) => {
    return (
      item.task.toLowerCase().includes(searchTask.toLowerCase()) ||
      item.description.toLowerCase().includes(searchTask.toLowerCase()) ||
      item.priority.toLowerCase().includes(searchTask.toLowerCase()) ||
      item.deadline.toLowerCase().includes(searchTask.toLowerCase())
    );
  });

  const filteredTodolist = filteredTask
    .sort((a, b) => a.priority - b.priority)
    .map((item) => {
      return (
        <div className="App__FilteredTodolist">
          <Task key={item} {...item} id={item.id} />

          <Button
            className="Task__ButtonDelete"
            variant="danger"
            onClick={() => deleteTodo(item.id)}
          >
            Delete todo
          </Button>
          <Edit onSubmit={(e) => editChange(e, item.id)} item={item} />
        </div>
      );
    });

  return (
    <div className="App__MainReturn">
      {filteredTodolist}

      <SearchBox handleInput={handleInput} />

      <AddNewTask onSubmit={(fields) => addTodo(fields)} />
    </div>
  );
}

export default App;
