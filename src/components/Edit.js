import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import { format } from "date-fns";
import { Button, InputGroup, FormControl, Form } from "react-bootstrap";
import ModalEditWindow from "./ModalEditWindow";

function Edit(props) {
  const { id, ...withoutId } = props.item;
  const [childVisible, setChildVisible] = useState(false);
  const [startDate, setStartDate] = useState(null);
  const [show, setShow] = useState(true);

  const [fields, setFields] = useState({
    ...props.item,
    childVisible,
    startDate,
    show,
  });

  useEffect(() => {
    setFields({
      ...props.item,
      task: props.item.task,
      description: props.item.description,
      priority: props.item.priority,
      deadline: props.item.deadline,
      done: props.item.done,
    });
  }, [props.item]);

  const openOrCloseEdit = () => {
    setChildVisible(!childVisible);
  };

  const dateTempChange = (handleChange) => {
    const arrDate = format(handleChange, "MM/dd/yyyy");
    setFields({ ...fields, deadline: arrDate, startDate: handleChange });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFields({ ...fields, [name]: value });
  };

  const onSubmit = (e) => {
    e.preventDefault();
    props.onSubmit({ ...fields });
    setChildVisible(false);
  };

  const inputs = Object.keys(withoutId).map((field, i) => {
    if (
      field === "startDate" ||
      field === "childVisible" ||
      field === "show" ||
      field === "openAddTodo"
    ) {
      return false;
    } else if (field === "deadline") {
      return (
        <InputGroup className="mb-3 Edit__InputGroup">
          <InputGroup.Prepend>
            <InputGroup.Text className="Edit__InputGroupText">
              {field}
            </InputGroup.Text>
          </InputGroup.Prepend>
          <DatePicker
            className="Edit__DatePicker"
            name="deadline"
            placeholderText="enter deadline"
            selected={startDate}
            onChange={(e) => dateTempChange(e)}
            value={fields[field]}
            customInput={<FormControl className="Edit__FormControl" />}
          ></DatePicker>
        </InputGroup>
      );
    } else if (field === "done") {
      return (
        <Form.Group className="Edit__FormGroup">
          <InputGroup>
            <InputGroup.Prepend>
              <InputGroup.Text className="Edit__InputGroupText">
                Select "done" or "in progress"
              </InputGroup.Text>
            </InputGroup.Prepend>

            <Form.Control
              as="select"
              placeholder="done"
              defaultValue="in progress"
              key={i}
              name={field}
              onChange={(e) => handleChange(e)}
              value={fields[field]}
            >
              <option>Choose</option>
              <option>Done</option>
              <option>In progress</option>
            </Form.Control>
          </InputGroup>
        </Form.Group>
      );
    } else {
      return (
        <InputGroup className="mb-3">
          <InputGroup.Prepend>
            <InputGroup.Text className="Edit__InputGroupText">
              {field}
            </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            key={i}
            placeholder={field}
            name={field}
            onChange={(e) => handleChange(e)}
            value={fields[field]}
          />
        </InputGroup>
      );
    }
  });

  return (
    <div>
      <Button
        className="Edit__button editTodo"
        variant="info"
        onClick={openOrCloseEdit}
      >
        Edit todo
      </Button>

      <br />

      {childVisible && (
        <ModalEditWindow
          show={show}
          inputs={inputs}
          onSubmit={onSubmit}
          openOrCloseEdit={openOrCloseEdit}
        />
      )}
    </div>
  );
}

export default Edit;
