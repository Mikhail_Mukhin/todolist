import React, { useState } from "react";
import { Button, Form, FormControl, InputGroup, Modal } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const ModalAddWindow = (props) => {
  const [show, setShow] = useState(true);

  const handleClose = () => {
    setShow(false);
    props.changeStateOpenAddTodo();
  };

  return (
    <div>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Fill in the fields</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form className="ModalAddWindow__Form">
            <InputGroup className="ModalAddWindow__InputGroup">
              <InputGroup.Prepend>
                <InputGroup.Text className="ModalAddWindow__InputGroupText">
                  Task
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="task"
                name="task"
                value={props.task}
                onChange={(e) => props.change(e)}
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup className="InputGroup">
              <InputGroup.Prepend>
              <InputGroup.Text className="ModalAddWindow__InputGroupText">
                  Description
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                placeholder="description"
                name="description"
                value={props.description}
                onChange={(e) => props.change(e)}
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup className="ModalAddWindow__InputGroup">
              <InputGroup.Prepend>
              <InputGroup.Text className="ModalAddWindow__InputGroupText">
                  Priority (numerically)
                </InputGroup.Text>
              </InputGroup.Prepend>
              <FormControl
                type="number"
                placeholder="priority"
                name="priority"
                value={props.priority}
                onChange={(e) => props.change(e)}
                aria-label="Default"
                aria-describedby="inputGroup-sizing-default"
              />
            </InputGroup>
            <InputGroup className="ModalAddWindow__InputGroup">
              <InputGroup.Prepend>
              <InputGroup.Text className="ModalAddWindow__InputGroupText">
                  Deadline
                </InputGroup.Text>
              </InputGroup.Prepend>
              <DatePicker className="ModalAddWindow__DatePicker"
                class="form-control"
                placeholderText="enter deadline"
                selected={props.startDate}
                onChange={(e) => props.onDateChange(e)}
                customInput={<FormControl className="ModalAddWindow__FormControl" />}
              />
            </InputGroup>
            <Form.Group className="ModalAddWindow__ForomGroup">
              <InputGroup>
                <InputGroup.Prepend>
                <InputGroup.Text className="ModalAddWindow__InputGroupText">
                    Select "done" or "in progress"
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                  as="select"
                  placeholder="done"
                  defaultValue="in progress"
                  name="done"
                  value={props.done}
                  onChange={(e) => props.change(e)}
                >
                  <option>Choose</option>
                  <option>Done</option>
                  <option>In progress</option>
                </Form.Control>
              </InputGroup>
            </Form.Group>

            <Button variant="success" onClick={(e) => props.onSubmit(e)}>
              Add todo
            </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default ModalAddWindow;
