import React from "react";
import { Table } from "react-bootstrap";

function Task(props) {
  return (
    <div className="Task__MainReturn">
      <Table striped bordered hover variant="dark" className="Task__Table">
        <thead >
          <tr>
            <th className="Task__TableCell" >Task: {props.task}</th>
            <th className="Task__TableCell" >Description: {props.description}</th>
            <th className="Task__TableCell" >Priority: {props.priority}</th>
            {new Date(props.deadline) -
              24 * 60 * 60 -
              (new Date() - 24 * 60 * 60) >
            259200000 ? (
              <th className="Task__TableCell TaskGreenDate" >
                Deadline: {props.deadline}
              </th>
            ) : (
              <th className="Task__TableCell TaskRedDate" >
                Deadline: {props.deadline}
              </th>
            )}

            <th className="Task__TableCell" >Done: {props.done}</th>
          </tr>
        </thead>
      </Table>
    
    </div>
  );
}

export default Task;
