import React, { useState } from "react";
import "react-datepicker/dist/react-datepicker.css";
import { format } from "date-fns";
import { Button } from "react-bootstrap";
import ModalAddWindow from "./ModaAddlWindow";


function AddNewTask(props) {

  const [openAddTodo, setOpenAddTodo] = useState(false);
  const [startDate, setStartDate] = useState(null);
  const [fields, setFields] = useState({
    task: "",
    description: "",
    priority: "",
    deadline: "",
    done: "",
    openAddTodo,
    startDate,
  });

  const openAddTodoClick = () => {
    setOpenAddTodo(!openAddTodo);
  };

  const onDateChange = (handleChange) => {
    const arrDate = format(handleChange, "MM/dd/yyyy");
    setFields({ ...fields, deadline: arrDate, startDate: handleChange });
  };

  const change = (e) => {
    const { name, value } = e.target;
    setFields({ ...fields, [name]: value });
  };

  const onSubmit = (e) => {
    e.preventDefault();

    props.onSubmit({ ...fields });
    setFields({
      startDate: null,
      task: "",
      description: "",
      priority: "",
      deadline: "",
      done: "",
    });
    setOpenAddTodo(!openAddTodo);
  };

  return (
    <div>
      <Button className="AddNewTask__Button"
        onClick={() => openAddTodoClick()}
      >
        Add task
      </Button>
      {openAddTodo && (
        <ModalAddWindow
          description={fields.description}
          priority={fields.priority}
          startDate={fields.startDate}
          done={fields.done}
          changeStateOpenAddTodo={openAddTodoClick}
          change={change}
          onDateChange={onDateChange}
          onSubmit={onSubmit}
        ></ModalAddWindow>
      )}
    </div>
  );
}

export default AddNewTask;
