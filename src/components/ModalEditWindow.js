import React from "react"
import { Button, Modal, } from "react-bootstrap";

function ModalEditWindow(props) {
  return (
    <div>
      
      <Modal show={props.show} onHide={props.openOrCloseEdit}>
        <Modal.Header closeButton>
          <Modal.Title>Fill in the fields</Modal.Title>
        </Modal.Header>
        <Modal.Body className="Edit__ModalBody">
          {props.inputs}
          <Button
            className="Edit__button"
            variant="primary"
            onClick={(e) => props.onSubmit(e)}
          >
            Submit
          </Button>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="secondary" onClick={props.openOrCloseEdit}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default ModalEditWindow;
