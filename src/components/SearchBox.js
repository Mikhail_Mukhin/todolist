import React from "react";
import { InputGroup, Form } from "react-bootstrap";

function SearchBox(props) {
  return (
    <div
      style={{
        width: '400px',
        display: "flex",
        margin: '20px'
      }}
    >
      <InputGroup>
        <InputGroup.Prepend>
          <InputGroup.Text id="inputGroup-sizing-default">
          Search by
          </InputGroup.Text>
        </InputGroup.Prepend>
        <Form.Control
          aria-label="Default"
          aria-describedby="inputGroup-sizing-default"
          onChange={props.handleInput} type="text" 
        />
      </InputGroup>
      <br />
      
    </div>
  );
}

export default SearchBox;
